# Original code
[https://github.com/yumeng5/WeSTClass](https://github.com/yumeng5/WeSTClass)

# Modifications
+ Allow to use abitrary integer to represent a class.
+ Make the model's prediction **reproducible**.
+ Add more arguments to ``main.py``
    + ``--pretrained_embedding``: allows to use a pretrained word embeddings model.
    + ``--cnn_num_filters``: set ``num_filter`` parameter for CNN model.
    + ``--cnn_filter_sizes``: set ``filter_sizes`` parameter for CNN model.

# Setup
+ Download [word2vec pretrained model](http://embeddings.net/frWac_non_lem_no_postag_no_phrase_200_cbow_cut100.bin)
+ ``pip install -r requirements.txt``
+ Want to compare or reproduce our results? Ask us for our datasets, that can be shared under conditions.

# Configuration of the best models
## C_s dataset
+ F1 score ``dev = 0.727, test = 0.711``
```
export PYTHONHASHSEED=0 && python main.py --dataset C_s \
    --max_doc_length 100 \
    --maxiter 500 \
    --model cnn \
    --pretrain_epochs 20 \
    --sup_source keywords
    --cnn_filter_sizes "[2,2,2,2]" \
    --use_pretrained_embedding True \
    --pretrained_embedding frWac_non_lem_no_postag_no_phrase_200_cbow_cut100.bin \
    --embedding_dim 200 \
    --cnn_num_filters 20
```
+ MAP@3 score ``dev = 0.738, test = 0.721``
```
export PYTHONHASHSEED=0 && python main.py --dataset C_s \
    --max_doc_length 100 \
    --maxiter 500 \
    --model cnn \
    --pretrain_epochs 20 \
    --sup_source keywords
    --cnn_filter_sizes "[2,2,2,2]" \
    --use_pretrained_embedding True \
    --pretrained_embedding frWac_non_lem_no_postag_no_phrase_200_cbow_cut100.bin \
    --embedding_dim 200 \
    --cnn_num_filters 60
```

## C_t dataset 
+ F1 score ``dev = 0.785, test = 0.817``
+  MAP@3 score `` dev = 0.823, test = 0.842``
```
export PYTHONHASHSEED=0 && python main.py --dataset C_s \
    --max_doc_length 50 \
    --maxiter 500 \
    --model cnn \
    --pretrain_epochs 20 \
    --sup_source keywords
    --cnn_filter_sizes "[2,3,4,5]" \
    --use_pretrained_embedding True \
    --pretrained_embedding frWac_non_lem_no_postag_no_phrase_200_cbow_cut100.bin \
    --embedding_dim 200 \
    --cnn_num_filters 100
```

# WeSTClass paper
```
@inproceedings{meng2018weakly,
  title={Weakly-Supervised Neural Text Classification},
  author={Meng, Yu and Shen, Jiaming and Zhang, Chao and Han, Jiawei},
  booktitle={Proceedings of the 27th ACM International Conference on Information and Knowledge Management},
  pages={983--992},
  year={2018},
  organization={ACM}
}
```
