import csv
import numpy as np
import pickle
import sys
import numpy as np
from collections import Counter
from sklearn.metrics import f1_score


def load_predictions(folder):
    dict_sentence_pred = dict()
    with open(f'{folder}/out.txt') as f:
        with open(f'{folder}/dataset.csv') as f2:
            sentences = f2.read().splitlines()
            preds = f.read().splitlines()
            for (sentence, pred) in zip(sentences, preds):
                dict_sentence_pred[sentence] = pred

    return dict_sentence_pred


def _labels_to_binary(map_label_to_id, labels, num_classes):
    result = [0] * num_classes
    for label in labels:
        result[map_label_to_id[label]] = 1
    return result


def multilabel_mapk(gold_standard_file, dict_sentence_pred, map_label_to_id):
    num_classes = len(map_label_to_id)
    list_y, list_y_pred = list(), list()
    # count_multi_label = 0
    with open(gold_standard_file) as f:
        for i, line in enumerate(f.read().splitlines()): 
            sentence = line[line.index(',') + 1:]
            preds = dict_sentence_pred[sentence].split('-')
            list_y_pred.append([int(p) for p in preds])

            label = line[: line.index(',')].split('-') 
            list_y.append([int(_y) for _y in label])

    return mapk(list_y, list_y_pred, k=3)


def multilabel_f1_score(gold_standard_file, dict_sentence_pred, map_label_to_id):
    num_classes = len(map_label_to_id)
    list_y, list_y_pred = list(), list()
    # count_multi_label = 0
    with open(gold_standard_file) as f:
        for i, line in enumerate(f.read().splitlines()): 
            sentence = line[line.index(',') + 1:]
            # preds = dict_sentence_pred[sentence].split('-')[: len(label.split('-'))]
            preds = dict_sentence_pred[sentence].split('-')[:1]
            y_pred = _labels_to_binary(map_label_to_id, preds, num_classes)
            list_y_pred.append(y_pred)

            label = line[: line.index(',')] 
            y = _labels_to_binary(map_label_to_id, label.split('-'), num_classes) 
            if preds[0] in label.split('-'):
                # print(preds, label)
                y = y_pred
            list_y.append(y)

    return f1_score(np.array(list_y), np.array(list_y_pred), average='micro')


def top_k_accuracy(gold_standard_file, dict_sentence_pred):
    y, y_pred = list(), list()
    counter = Counter()
    counter_pred = Counter()
    correct, total = 0, 0
    # current_progress = 266
    with open(gold_standard_file) as f:
        for i, line in enumerate(f.read().splitlines()):
            label = line[: line.index(',')]
            total += len(label.split('-'))

            y = list()
            for _y in label.split('-'):
                y.append(_y)

            sentence = line[line.index(',') + 1:]
            preds = dict_sentence_pred[sentence]
            got_correct = False
            for pred in preds.split('-'):
                for _y in y:
                    if pred == _y:
                        correct += 1

    return correct / total


def apk(actual, predicted, k=10):
    """
    Computes the average precision at k.
    This function computes the average prescision at k between two lists of
    items.
    Parameters
    ----------
    actual : list
             A list of elements that are to be predicted (order doesn't matter)
    predicted : list
                A list of predicted elements (order does matter)
    k : int, optional
        The maximum number of predicted elements
    Returns
    -------
    score : double
            The average precision at k over the input lists
    """
    if len(predicted)>k:
        predicted = predicted[:k]

    score = 0.0
    num_hits = 0.0

    for i,p in enumerate(predicted):
        if p in actual and p not in predicted[:i]:
            num_hits += 1.0
            score += num_hits / (i+1.0)

    if not actual:
        return 0.0

    return score / min(len(actual), k)

def mapk(actual, predicted, k=10):
    """
    Computes the mean average precision at k.
    This function computes the mean average prescision at k between two lists
    of lists of items.
    Parameters
    ----------
    actual : list
             A list of lists of elements that are to be predicted 
             (order doesn't matter in the lists)
    predicted : list
                A list of lists of predicted elements
                (order matters in the lists)
    k : int, optional
        The maximum number of predicted elements
    Returns
    -------
    score : double
            The mean average precision at k over the input lists
    """
    return np.mean([apk(a,p,k) for a,p in zip(actual, predicted)])
