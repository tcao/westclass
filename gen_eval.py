from eval import load_predictions, multilabel_f1_score, multilabel_mapk
import sys


def _map_label_to_id(keywords_file):
    with open(keywords_file) as f:
        _map = dict()
        for _id, line in enumerate(f.read().splitlines()):
            _map[line.split(':')[0]] = _id
        return _map

model_name = sys.argv[1]
indices = sys.argv[2]
metric = sys.argv[3]
start, end = indices.split(',')
best_dev = 0
best_model = None
for i in range(int(start), int(end) + 1):
    model = f'{model_name}{i}'
    log_file = f'{model}/log'
    is_experiment_finished = False
    with open(log_file) as f:
        if 'Classification results are written in' in f.read():
            is_experiment_finished = True
    if is_experiment_finished:
        if 'C_s' in model:
            prefix = 'sentences'
            _map = _map_label_to_id('keywords/sentence_keywords.txt')
        elif 'C_t' in model:
            prefix = 'tweets'
            _map = _map_label_to_id('keywords/tweet_keywords.txt')
        if 'f1' == metric:
            score_dev = multilabel_f1_score(f"{prefix}_dev.csv", load_predictions(model), _map)
            score_test = multilabel_f1_score(f"{prefix}_test.csv", load_predictions(model), _map)
        elif 'mapk' == metric:
            score_dev = multilabel_mapk(f"{prefix}_dev.csv", load_predictions(model), _map)
            score_test = multilabel_mapk(f"{prefix}_test.csv", load_predictions(model), _map)
        print((' ' if i <  10 else '') + f'{model}: dev = {score_dev:.3f}, test = {score_test:.3f}')

        if score_dev > best_dev:
            best_dev = score_dev
            best_model = i
    else:
        print((' ' if i <  10 else '') + f'{model}: still running')

print('Best model:', best_model)
